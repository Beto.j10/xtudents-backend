package main

import (
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/acme/autocert"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/sync/errgroup"

	"backend/api/answers"
	"backend/api/ask"
	"backend/api/edit"
	"backend/api/feedback"
	"backend/api/forms"
	"backend/api/home"
	"backend/api/login"
	"backend/api/profile"
	"backend/api/questions"
	"backend/api/reply"
	"backend/api/signup"
	db "backend/database"
)

func main() {

	isDev := os.Getenv("GO_ENV") == "development"
	db.Init()
	//Enable CORS
	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"https://xtudents.com", "https://test.xtudents.com", "https://test-core.xtudents.com"},
		AllowCredentials: true,
		// AllowHeaders:     []string{"Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "accept", "origin", "Cache-Control", "X-Requested-With"},
		AllowHeaders: []string{"Content-Type", "Authorization"},
		AllowMethods: []string{"POST", "GET"},
	}))

	signup.Signup(router)
	login.Login(router)
	home.Home(router)
	ask.Ask(router)
	reply.Reply(router)
	profile.Profile(router)
	answers.Answers(router)
	questions.Questions(router)
	feedback.Feedback(router)
	forms.Forms(router)
	edit.Edit(router)

	router.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	if isDev {
		log.Println("xtudents Backend started and listening")
		log.Fatal(router.Run(":8080"))
	} else {
		var g errgroup.Group

		m := autocert.Manager{
			Prompt: autocert.AcceptTOS,
			Cache:  autocert.DirCache("/cert-cache"),
			HostPolicy: autocert.HostWhitelist(
				"buscadorlici.de"),
		}

		g.Go(func() error {
			return http.ListenAndServe(":80", m.HTTPHandler(http.HandlerFunc(redirect)))
		})
		g.Go(func() error {
			serve := &http.Server{
				Addr:    ":443",
				Handler: router,
				TLSConfig: &tls.Config{
					GetCertificate: m.GetCertificate,
					// NextProtos:     []string{"http/1.1"}, // disable h2 because Safari :(
				},
			}
			return serve.ListenAndServeTLS("", "")
		})
		log.Println("Xtudents Backend started and listening on port ")

		log.Fatal(g.Wait())
	}

}

func redirect(w http.ResponseWriter, req *http.Request) {
	var serverHost = "xtudents.now.sh"
	serverHost = strings.TrimPrefix(serverHost, "http://")
	serverHost = strings.TrimPrefix(serverHost, "https://")
	req.URL.Scheme = "https"
	req.URL.Host = serverHost

	w.Header().Set("Strict-Transport-Security", "max-age=31536000")

	http.Redirect(w, req, req.URL.String(), http.StatusMovedPermanently)
}

// 	err = checkmail.ValidateFormat(email)
// 	if err != nil {
// 		return 0, errors.New("bad email format")
// 	}

// }

func checkPasswordHash(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

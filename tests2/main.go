package main

import "fmt"

func main() {

	vars := `WHERE categories_id = $1
	ORDER BY created_at DESC`

	queryString := `SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id, questions.created_at, questions.likes, questions.dislikes, questions.anonymous, users.nickname, users.countries_id
	FROM questions
		INNER JOIN users ON questions.users_id = users.id
	` + vars

	fmt.Println(queryString)
}

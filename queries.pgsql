SELECT id, category FROM "categories"
ORDER BY category;


    SELECT id, answer, users_id, questions_id, created_at, likes, dislikes
	FROM public.answers
	ORDER BY likes DESC, created_at DESC
	LIMIT 1;


		SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id, questions.created_at, questions.likes, questions.dislikes, questions.anonymous, users.nickname, users.countries_id
		FROM questions
			INNER JOIN users ON questions.users_id = users.id
		WHERE categories_id = 73
		ORDER BY created_at DESC


-- tabla answers con usuario(nickname)que la respondió. 

    SELECT answers.id, answers.answer, answers.users_id, answers.questions_id, answers.created_at, answers.likes, answers.dislikes, users.nickname
	FROM public.answers
	INNER JOIN users ON answers.users_id = users.id
	ORDER BY likes DESC, created_at DESC
	LIMIT 1;


    



-- Query para card, junto con la primera respuesta con mas likes o en su defecto la mas reciente junto con el usuario que la respondió.
		-- SELECT selecciona todos los campos que se devolveran en el query. Pregunta, usuario que realizo la pregunta, una respuesta y usuario que realizó la respuesta.
		SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id, questions.created_at, questions.likes, questions.dislikes, questions.anonymous, users.nickname, users.countries_id, answersTable.answersid AS answer_id, answersTable.answer, answersTable.answersUsersID AS answer_users_id, answersTable.answersQuestionsID AS answer_questions_id, answersTable.created_at, answersTable.answersLikes AS answer_likes, answersTable.answersDislikes AS answer_dislikes, answersTable.answersUsersNickname AS answer_users_nickname
		FROM questions
			-- INNER JOIN para traer el usuario que está realizando la pregunta
			INNER JOIN users ON questions.users_id = users.id
			-- INNER JOIN para unir la pregunta con una respuesta a esta
			INNER JOIN (SELECT answers.id AS answersID, answers.answer, answers.users_id AS answersUsersID, answers.questions_id AS answersQuestionsID, answers.created_at, answers.likes AS answersLikes, answers.dislikes AS answersDislikes, users.nickname AS answersUsersNickname
						FROM public.answers
						-- INNER JOIN trae el usuaio que respondió
						INNER JOIN users ON answers.users_id = users.id
						-- ORDER BY ordena por mas likes o más reciente si hay varios con los mismos likes
						ORDER BY likes DESC, created_at DESC
						-- LIMIT selecciona sólo una respuesta; la primera.
						LIMIT 1) AS answersTable ON answersTable.answersQuestionsID = questions.id
		WHERE categories_id = 73
		-- ORDER BY ordena las preguntas de la más reciente
		ORDER BY created_at DESC;



-- Query original home

		SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id, questions.created_at, questions.likes, questions.dislikes, questions.anonymous, users.nickname, users.countries_id
		FROM questions
			INNER JOIN users ON questions.users_id = users.id
		WHERE categories_id = $1
		ORDER BY created_at DESC



		, &question.AnswerID, &question.Answer, &question.AnswerUsersID, &question.AnswerQuestionsID, &question.AnswerDate, &question.AnswerLikes, &question.AnswerDislikes, &question.AnswerUsersNickname

	AnswerID            int64     `db:"answer_id"`
	Answer              string    `db:"answer"`
	AnswerUsersID       int       `db:"answer_users_id"`
	AnswerQuestionsID   int       `db:"answer_questions_id"`
	AnswerDate          time.Time `db:"created_at"`
	AnswerLikes         int       `db:"answer_likes"`
	AnswerDislikes      int       `db:"answer_dislikes"`
	AnswerUsersNickname string    `db:"answer_users_nickname"`


	-- query con numero de comentarios sin tabla answers

			SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id,
		questions.created_at, questions.likes, questions.dislikes,
		questions.anonymous, users.nickname, users.countries_id, 
			(SELECT COUNT(questions_id) quantity_answers
			FROM answers
			WHERE questions_id = questions.id)
		FROM questions
			INNER JOIN users ON questions.users_id = users.id
		ORDER BY created_at DESC
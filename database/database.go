package database

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"
)

// Pool *pgxpool.Pool
var Pool *pgxpool.Pool

// Init database
func Init() {

	host := `host=` + os.Getenv("DB_HOST") +
		` user=` + os.Getenv("DB_USER") +
		` password=` + os.Getenv("DB_PASSWORD") +
		` database=` + os.Getenv("DB_NAME")

	// host := "host=35.188.135.221 user=postgres password=DATABASEXTUDENTSbeto1+ database=postgres"

	var err error
	Pool, err = pgxpool.Connect(context.Background(), host)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connection to database: %v\n", err)
		os.Exit(1)
	}
}

CREATE TABLE public.users
(
    id serial NOT NULL,
    email character varying NOT NULL,
    first_name character varying,
    last_name character varying,
    password_hash character varying NOT NULL,
    nickname character varying NOT NULL,
    institution character varying NOT NULL,
    career character varying,
    countries_id integer NOT NULL,
    score integer NOT NULL DEFAULT 50,
    avatar character varying NOT NULL,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT email_uq UNIQUE (email)
);

ALTER TABLE public.users
    OWNER to postgres;


CREATE TABLE public.questions
(
    id bigserial NOT NULL,
    title character varying NOT NULL,
    content character varying NOT NULL,
    users_id integer NOT NULL,
    categories_id integer NOT NULL,
    anonymous boolean NOT NULL DEFAULT false,
    file bytea,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT questions_users FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.questions
    OWNER to postgres;


    CREATE TABLE public.answers
(
    id bigserial NOT NULL,
    answer character varying NOT NULL,
    users_id integer NOT NULL,
    questions_id bigserial NOT NULL,
    created_id timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT answers_users FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT answers_questions FOREIGN KEY (questions_id)
        REFERENCES public.questions (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.answers
    OWNER to postgres;



CREATE TABLE public.likes_questions
(
    id bigserial NOT NULL,
    questions_id bigint NOT NULL,
    users_id integer NOT NULL,
    checked boolean NOT NULL DEFAULT false,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT likes_questions_questions_id FOREIGN KEY (questions_id)
        REFERENCES public.questions (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT likes_questions_users_id FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.likes_questions
    OWNER to postgres;




CREATE TABLE public.favorites
(
    id bigserial NOT NULL,
    questions_id bigint NOT NULL,
    users_id integer NOT NULL,
    checked boolean NOT NULL DEFAULT true,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT favorites_questions_id FOREIGN KEY (questions_id)
        REFERENCES public.questions (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT favorites_users_id FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.favorites
    OWNER to postgres;




CREATE TABLE public.likes_answers
(
    id bigserial NOT NULL,
    answers_id bigint NOT NULL,
    users_id bigint NOT NULL,
    checked boolean NOT NULL DEFAULT true,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT likes_answers_answers_id FOREIGN KEY (answers_id)
        REFERENCES public.questions (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT likes_answers_users_id FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.likes_answers
    OWNER to postgres;




CREATE TABLE public.feedback
(
    id bigserial NOT NULL,
    users_id integer NOT NULL,
    content character varying NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT feedback_users_id FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.feedback
    OWNER to postgres;




CREATE TABLE public.form_skills
(
    id bigserial NOT NULL,
    users_id integer NOT NULL,
    name character varying NOT NULL,
    phone character varying NOT NULL,
    age integer NOT NULL,
    email character varying NOT NULL,
    content character varying NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT form_skills_users_id FOREIGN KEY (users_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

ALTER TABLE public.form_skills
    OWNER to postgres;
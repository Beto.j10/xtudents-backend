package edit

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	db "backend/database"

	"github.com/gin-gonic/gin"
)

//Edit user edit
func Edit(router *gin.Engine) {
	routerEdit := router.Group("/edit")
	{

		routerEdit.GET("", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "edit"})
		})

		routerEdit.POST("/question", func(c *gin.Context) {

			questionIDString := c.Request.FormValue("id")
			title := c.Request.FormValue("title")
			content := c.Request.FormValue("content")

			fmt.Println("questionIDString: ", questionIDString, " answer: ", title, " content: ", content)

			//convert string to int
			questionID, err := strconv.Atoi(questionIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			id, err := editQuestion(questionID, title, content)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"modify": id})

		})
		routerEdit.POST("/answer", func(c *gin.Context) {

			answerIDString := c.Request.FormValue("id")
			answer := c.Request.FormValue("answer")

			fmt.Println("answerIDString: ", answerIDString, " answer: ", answer)

			//convert string to int
			answerID, err := strconv.Atoi(answerIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			id, err := editAnswer(answerID, answer)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"modify": id})

		})
	}

}

func editQuestion(questionID int, title string, content string) (int64, error) {
	//QueryRow no return Rows, simple Row.

	var id int64
	err := db.Pool.QueryRow(context.Background(), `
	UPDATE public.questions
		SET title=$2, content = $3
	WHERE id = $1 RETURNING id
		`, questionID, title, content).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil

}
func editAnswer(answerID int, answer string) (int64, error) {
	//QueryRow no return Rows, simple Row.

	var id int64
	err := db.Pool.QueryRow(context.Background(), `
	UPDATE public.answers
		SET answer=$2
	WHERE id = $1 RETURNING id
		`, answerID, answer).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil

}

package forms

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	db "backend/database"

	"github.com/gin-gonic/gin"
)

//Forms user forms
func Forms(router *gin.Engine) {
	routerForms := router.Group("/forms")
	{

		routerForms.GET("", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "forms"})
		})

		routerForms.POST("/skills", func(c *gin.Context) {

			userIDString := c.Request.FormValue("user")
			name := c.Request.FormValue("name")
			phone := c.Request.FormValue("phone")
			ageString := c.Request.FormValue("age")
			email := c.Request.FormValue("email")
			content := c.Request.FormValue("content")

			fmt.Println("userID: ", userIDString, " name: ", name, " phone: ", phone, " age: ", ageString, " email ", email, " content: ", content)

			if userIDString == "0" {
				userIDString = "60"
			}

			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			age, err := strconv.Atoi(ageString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			id, err := sendSkills(userID, name, phone, age, email, content)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"id": id})

		})
	}

}

func sendSkills(userID int, name string, phone string, age int, email string, content string) (int64, error) {
	//QueryRow no return Rows, simple Row.

	var id int64
	err := db.Pool.QueryRow(context.Background(), `
	INSERT INTO form_skills(
		users_id, name, phone, age, email, content)
		VALUES ( $1, $2, $3, $4, $5, $6) RETURNING id
		`, userID, name, phone, age, email, content).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil

	// query returns the quantity of answers

	// err = db.Pool.QueryRow(context.Background(), `
	// SELECT COUNT(questions_id) AS quantity_answers
	// FROM answers
	// WHERE questions_id = $1
	// `, questionID).Scan(&quantityAnswers)
	// if err != nil {
	// 	return 0, err
	// }
	// return quantityAnswers, nil

}

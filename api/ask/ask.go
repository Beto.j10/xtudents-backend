package ask

import (
	"backend/api/login"
	"context"
	"fmt"
	"net/http"
	"strconv"

	db "backend/database"

	"github.com/gin-gonic/gin"
)

//Ask user question
func Ask(router *gin.Engine) {
	routerAsk := router.Group("/ask")
	routerAsk.Use(login.AuthMiddleware.MiddlewareFunc())
	{

		routerAsk.GET("", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "Ask"})
		})

		routerAsk.POST("", func(c *gin.Context) {

			userIDString := c.Request.FormValue("user")
			categoryString := c.Request.FormValue("category")
			title := c.Request.FormValue("title")
			question := c.Request.FormValue("question")
			fileString := c.Request.FormValue("file")

			fmt.Println("userID: ", userIDString, " categoryString: ", categoryString, " title: ", title, " question: ", question, "file: ", fileString)

			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			category, err := strconv.Atoi(categoryString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}

			byteFile := []byte(fileString)

			fmt.Println("fileByte: ", byteFile)

			id, err := createQuestion(title, question, userID, category, byteFile)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"id": id})

		})
	}

}

func createQuestion(title string, question string, userID int, category int, byteFile []byte) (int, error) {
	var id int
	//QueryRow no return Rows, simple Row.
	err := db.Pool.QueryRow(context.Background(), `
	INSERT INTO questions(
		title, content, users_id, categories_id, file)
		VALUES ( $1, $2, $3, $4, $5) RETURNING id
		`, title, question, userID, category, byteFile).Scan(&id)
	if err != nil {
		return 0, err
	}
	// defer rows.Close()
	// var users []interface{}
	// for rows.Next() {
	// 	err := rows.Scan(&id.id)
	// 	if err != nil {
	// 		return 0, err
	// 	}
	// 	users = append(users, id)
	// }
	return id, nil

}

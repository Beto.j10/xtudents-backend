package feedback

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	db "backend/database"

	"github.com/gin-gonic/gin"
)

//Feedback user feedback
func Feedback(router *gin.Engine) {
	routerFeedback := router.Group("/feedback")
	{

		routerFeedback.GET("", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "Feedback"})
		})

		routerFeedback.POST("", func(c *gin.Context) {

			userIDString := c.Request.FormValue("user")
			feedback := c.Request.FormValue("feedback")

			fmt.Println("userID: ", userIDString, " feedback: ", feedback)

			if userIDString == "0" {
				userIDString = "60"
			}

			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			id, err := createFeedback(userID, feedback)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"id": id})

		})
	}

}

func createFeedback(userID int, feedback string) (int64, error) {
	//QueryRow no return Rows, simple Row.

	var id int64
	err := db.Pool.QueryRow(context.Background(), `
	INSERT INTO feedback(
		users_id, content)
		VALUES ( $1, $2) RETURNING id
		`, userID, feedback).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil

	// query returns the quantity of answers

	// err = db.Pool.QueryRow(context.Background(), `
	// SELECT COUNT(questions_id) AS quantity_answers
	// FROM answers
	// WHERE questions_id = $1
	// `, questionID).Scan(&quantityAnswers)
	// if err != nil {
	// 	return 0, err
	// }
	// return quantityAnswers, nil

}

package profile

import (
	"backend/api/login"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	db "backend/database"
)

type user struct {
	ID          int    `db:"id"`
	NickName    string `db:"nickname"`
	Institution string `db:"institution"`
	Career      string `db:"career"`
	Country     int    `db:"countries_id"`
	Score       int64  `db:"score"`
	FirstName   string `db:"first_name"`
	LastName    string `db:"last_name"`
	Email       string `db:"email"`
	Avatar      string `db:"avatar"`
}

type favorite struct {
	QuestionID        int64     `db:"question_id"`
	QuestionTitle     string    `db:"question_title"`
	QuestionContent   string    `db:"question_content"`
	QuestionUserID    int       `db:"question_user_id"`
	QuestionCategory  int       `db:"categories_id"`
	QuestionCreatedAt time.Time `db:"question_created_at"`
	FavoriteCreatedAt time.Time `db:"favorite_created_at"`
	Nickname          string    `db:"nickname"`
	UserCountryID     int       `db:"user_country_id"`
	Avatar            string    `db:"avatar"`
	QuantityAnswers   int64     `db:"quantity_answers"`
	QuantityLikes     int64     `db:"quantity_likes"`
}
type question struct {
	QuestionID       int64     `db:"id"`
	QuestionTitle    string    `db:"title"`
	QuestionContent  string    `db:"content"`
	QuestionCategory int       `db:"categories_id"`
	CreatedAt        time.Time `db:"created_at"`
	UpdatedAt        time.Time `db:"updated_at"`
	QuantityAnswers  int64     `db:"quantity_answers"`
	QuantityLikes    int64     `db:"quantity_likes"`
}

type interfaces struct {
	Favorites []interface{}
	Questions []interface{}
}

//Profile user profile
func Profile(router *gin.Engine) {
	routerProfile := router.Group("/profile").Use(login.AuthMiddleware.MiddlewareFunc())
	// routerProfile.Use(login.AuthMiddleware.MiddlewareFunc())
	{
		routerProfile.GET("", func(c *gin.Context) {

			userIDString := c.Query("user")
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			userProfile, err := getProfile(userID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"user": userProfile,
			})

		})
		routerProfile.GET("/library", func(c *gin.Context) {

			userIDString := c.Query("user")
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			library, err := getLibrary(userID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"library": library,
			})

		})
	}
}

func getProfile(userID int) (user, error) {
	var user user
	err := db.Pool.QueryRow(context.Background(), `
	SELECT 
	id, nickname, email, first_name, last_name, institution, career, countries_id, score, avatar
	FROM public.users
	WHERE id = $1
	`, userID).Scan(&user.ID, &user.NickName, &user.Email, &user.FirstName, &user.LastName, &user.Institution, &user.Career, &user.Country, &user.Score, &user.Avatar)

	if err != nil {
		return user, err
	}
	return user, nil
}

func getLibrary(userID int) ([]interface{}, error) {
	var favoritesInterface []interface{}
	var favorite favorite
	rows, err := db.Pool.Query(context.Background(), `
	SELECT questions.id AS question_id, questions.title AS question_title, questions.content AS question_content, questions.users_id AS question_user_id, 
		questions.categories_id, questions.created_at AS question_created_at, favorites.created_at AS favorite_created_at,
		users.nickname, users.countries_id AS user_country_id, users.avatar,
		(SELECT COUNT(questions_id) quantity_answers
		FROM answers
		WHERE questions_id = questions.id),
		(SELECT COUNT(questions_id) quantity_likes
		FROM likes_questions
		WHERE questions_id = questions.id)
	FROM favorites
	INNER JOIN questions ON favorites.questions_id = questions.id
	INNER JOIN users ON questions.users_id = users.id
	WHERE favorites.users_id = $1
	ORDER BY favorites.created_at DESC
	`, userID)

	if err != nil {
		return favoritesInterface, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&favorite.QuestionID, &favorite.QuestionTitle, &favorite.QuestionContent, &favorite.QuestionUserID, &favorite.QuestionCategory, &favorite.QuestionCreatedAt, &favorite.FavoriteCreatedAt, &favorite.Nickname, &favorite.UserCountryID, &favorite.Avatar, &favorite.QuantityAnswers, &favorite.QuantityLikes)

		if err != nil {
			return favoritesInterface, err
		}
		favoritesInterface = append(favoritesInterface, favorite)
	}

	// ______________________________________________________________________________________________

	var questionsInterface []interface{}
	var question question
	rows2, err := db.Pool.Query(context.Background(), `
	SELECT id, title, content, categories_id, created_at, updated_at,
		(SELECT COUNT(questions_id) quantity_answers
		FROM answers
		WHERE questions_id = questions.id),
		(SELECT COUNT(questions_id) quantity_likes
		FROM likes_questions
		WHERE questions_id = questions.id)
	FROM public.questions
	WHERE users_id = $1
	ORDER BY created_at DESC;
	`, userID)

	if err != nil {
		return questionsInterface, err
	}
	defer rows2.Close()

	for rows2.Next() {
		err := rows2.Scan(&question.QuestionID, &question.QuestionTitle, &question.QuestionContent, &question.QuestionCategory, &question.CreatedAt, &question.UpdatedAt, &question.QuantityAnswers, &question.QuantityLikes)

		if err != nil {
			return questionsInterface, err
		}
		questionsInterface = append(questionsInterface, question)
	}

	var interfaces interfaces

	interfaces.Questions = questionsInterface
	interfaces.Favorites = favoritesInterface

	var libraryInterface []interface{}

	libraryInterface = append(libraryInterface, interfaces)

	return libraryInterface, nil
}

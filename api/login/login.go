package login

import (
	"os"

	"github.com/gin-gonic/gin"
)

// func helloHandler(c *gin.Context) {
// 	claims := jwt.ExtractClaims(c)
// 	user, _ := c.Get(identityKey)
// 	c.JSON(200, gin.H{
// 		"userID":   claims[identityKey],
// 		"userName": user.(*User).UserName,
// 		"text":     "Hello World.",
// 	})
// }

// user struct
type user struct {
	ID           int    `db:"id"`
	NickName     string `db:"nickname"`
	Institution  string `db:"institution"`
	Career       string `db:"career"`
	Country      int    `db:"countries_id"`
	Score        int64  `db:"score"`
	FirstName    string `db:"first_name"`
	LastName     string `db:"last_name"`
	Email        string `db:"email"`
	PasswordHash string `db:"password_hash"`
}

// Login lgin user
func Login(router *gin.Engine) {
	port := os.Getenv("PORT")

	if port == "" {
		port = "8000"
	}

	InitMiddleware()

	router.POST("/login", AuthMiddleware.LoginHandler)

	// router.NoRoute(AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {
	// 	claims := jwt.ExtractClaims(c)
	// 	log.Printf("NoRoute claims: %#v\n", claims)
	// 	c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	// })

	// auth := router.Group("/auth")
	// // Refresh time can be longer than token timeout
	// auth.GET("/refresh_token", AuthMiddleware.RefreshHandler)
	// auth.Use(AuthMiddleware.MiddlewareFunc())
	// {
	// 	auth.GET("/hello", helloHandler)
	// }

	// if err := http.ListenAndServe(":"+port, router); err != nil {
	// 	log.Fatal(err)
	// }
}

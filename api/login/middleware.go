package login

import (
	db "backend/database"
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type login struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

var identityKey = "id"

type userData struct {
	ID        int
	FirstName string
	LastName  string
	Email     string
}

// AuthMiddleware auth middleware, used for modules that wanna be protected
var AuthMiddleware *jwt.GinJWTMiddleware

// InitMiddleware authentication middleware
func InitMiddleware() {
	// the jwt middleware
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte("secret key"),
		Timeout:     (24 * 30) * time.Hour,
		MaxRefresh:  (24 * 30) * time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*userData); ok {
				return jwt.MapClaims{
					"ID":        v.ID,
					"Email":     v.Email,
					"FirstName": v.FirstName,
					"LastName":  v.LastName,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &userData{
				ID:        int(claims["ID"].(float64)),
				Email:     claims["Email"].(string),
				FirstName: claims["FirstName"].(string),
				LastName:  claims["LastName"].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			userEmail := loginVals.Email
			password := loginVals.Password

			user, err := authenticate(userEmail, password)
			if err != nil {
				// return nil, jwt.ErrFailedAuthentication
				return nil, err
			}
			c.Keys = map[string]interface{}{
				"UserID":      user.ID,
				"Email":       user.Email,
				"NickName":    user.NickName,
				"FirstName":   user.FirstName,
				"LastName":    user.LastName,
				"Institution": user.Institution,
				"Career":      user.Career,
				"Country":     user.Country,
				"Score":       user.Score,
			}
			return &userData{
				ID:        user.ID,
				Email:     user.Email,
				FirstName: user.FirstName,
				LastName:  user.LastName,
			}, nil

		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if _, ok := data.(*userData); ok {
				return true
			}

			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
		LoginResponse: func(c *gin.Context, code int, token string, expire time.Time) {
			ID, ok := c.Keys["UserID"].(int)
			if !ok {
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": "error in LoginResponse, UserID not found",
				})
				return
			}
			Email, _ := c.Keys["Email"].(string)
			NickName, _ := c.Keys["NickName"].(string)
			FirstName, _ := c.Keys["FirstName"].(string)
			LastName, _ := c.Keys["LastName"].(string)
			Institution, _ := c.Keys["Institution"].(string)
			Career, _ := c.Keys["Career"].(string)
			Country, _ := c.Keys["Country"].(int)
			Score, _ := c.Keys["Score"].(int64)

			c.JSON(http.StatusOK, gin.H{
				"ID":          ID,
				"FirstName":   FirstName,
				"LastName":    LastName,
				"Email":       Email,
				"NickName":    NickName,
				"Institution": Institution,
				"Career":      Career,
				"Country":     Country,
				"Score":       Score,
				"token":       token,
				"expire":      expire.Format(time.RFC3339),
			})
		},
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}

	AuthMiddleware = authMiddleware

}

func authenticate(email string, password string) (user, error) {
	var user user
	err := db.Pool.QueryRow(context.Background(), `
	SELECT 
	id, nickname, email, first_name, last_name, password_hash, institution, career, countries_id, score
	FROM public.users
	WHERE email = $1
	`, email).Scan(&user.ID, &user.NickName, &user.Email, &user.FirstName, &user.LastName, &user.PasswordHash, &user.Institution, &user.Career, &user.Country, &user.Score)

	if err != nil {
		if err.Error() == "no rows in result set" {
			err := errors.New("unregistered email")
			return user, err
		}
		return user, err
	}

	err = checkPasswordHash(password, user.PasswordHash)
	if err != nil {
		return user, err
	}
	return user, nil
}

func checkPasswordHash(password string, hash string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		err := errors.New("incorrect password")
		return err
	}
	return nil
}

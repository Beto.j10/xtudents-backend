package answers

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"backend/api/login"
	db "backend/database"
)

//GetAnswers struct query
type GetAnswers struct {
	ID           int16     `db:"id"`
	Content      string    `db:"answer"`
	UserID       int       `db:"users_id"`
	QuestionID   int64     `db:"questions_id"`
	CreatedAt    time.Time `db:"created_at"`
	UserNickName string    `db:"user_nickname"`
	UserCountry  int       `db:"user_country"`
	UserScore    int       `db:"user_score"`
	UserAvatar   string    `db:"user_avatar"`
	Likes        int       `db:"quantity_likes"`
	LikeChecked  bool      `db:"like_checked"`
}

//Answers users answers
func Answers(router *gin.Engine) {
	routerAnswers := router.Group("/answers")
	// routerAnswers.Use(login.AuthMiddleware.MiddlewareFunc())
	{
		routerAnswers.GET("", func(c *gin.Context) {
			questionIDString := c.Query("question")
			questionID, err := strconv.ParseInt(questionIDString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			answers, err := getAnswers(questionID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"answers": answers})
		})
		routerAnswers.GET("/auth", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {
			userIDString := c.Query("user")
			questionIDString := c.Query("question")
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			questionID, err := strconv.ParseInt(questionIDString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			answers, err := getAnswersAuth(userID, questionID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{
				"answers": answers,
			})
		})
		routerAnswers.POST("/add-marked", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {
			answerIDString := c.Request.FormValue("answer")
			userIDString := c.Request.FormValue("user")
			fmt.Println("answer: ", answerIDString, " user: ", userIDString)

			err := insertMarked(answerIDString, userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"succes": "ok"})

		})
		routerAnswers.POST("/delete-marked", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {

			answerIDString := c.Request.FormValue("answer")
			userIDString := c.Request.FormValue("user")
			fmt.Println("answer: ", answerIDString, " user: ", userIDString)

			//convert string to int64
			answerID, err := strconv.ParseInt(answerIDString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}

			err = deleteMarked(answerID, userID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"succes": "ok"})

		})
	}
}

func getAnswers(questionID int64) ([]interface{}, error) {

	var answersInterface []interface{}

	rows, err := db.Pool.Query(context.Background(), `
	SELECT answers.id, answer, users_id, questions_id, answers.created_at, 
		users.nickname AS user_nickname, users.countries_id AS user_country, users.score AS user_score, users.avatar AS user_avatar,
		(SELECT COUNT(answers_id) quantity_likes
		FROM likes_answers
		WHERE answers_id = answers.id)
	FROM public.answers
	INNER JOIN users ON answers.users_id = users.id
	WHERE questions_id= $1
	ORDER BY quantity_likes DESC, created_at DESC;
	`, questionID)

	if err != nil {
		return answersInterface, err
	}
	defer rows.Close()

	var answers GetAnswers
	for rows.Next() {
		err := rows.Scan(&answers.ID, &answers.Content, &answers.UserID, &answers.QuestionID, &answers.CreatedAt, &answers.UserNickName, &answers.UserCountry, &answers.UserScore, &answers.UserAvatar, &answers.Likes)
		if err != nil {
			return answersInterface, err
		}
		answersInterface = append(answersInterface, answers)
	}

	return answersInterface, nil
}
func getAnswersAuth(userID int, questionID int64) ([]interface{}, error) {

	var answersInterface []interface{}

	rows, err := db.Pool.Query(context.Background(), `
	SELECT answers.id, answer, users_id, questions_id, answers.created_at,
		users.nickname AS user_nickname, users.countries_id AS user_country, users.score AS user_score, users.avatar AS user_avatar,
		(SELECT COUNT(answers_id) quantity_likes
		FROM likes_answers
		WHERE answers_id = answers.id),
		COALESCE((SELECT checked
		FROM likes_answers
		WHERE answers_id = answers.id AND users_id = $1
		LIMIT 1), false) AS like_checked
	FROM public.answers
	INNER JOIN users ON answers.users_id = users.id
	WHERE questions_id= $2
	ORDER BY quantity_likes DESC, created_at DESC;
	`, userID, questionID)

	if err != nil {
		return answersInterface, err
	}
	defer rows.Close()

	var answers GetAnswers
	for rows.Next() {
		err := rows.Scan(&answers.ID, &answers.Content, &answers.UserID, &answers.QuestionID, &answers.CreatedAt, &answers.UserNickName, &answers.UserCountry, &answers.UserScore, &answers.UserAvatar, &answers.Likes, &answers.LikeChecked)
		if err != nil {
			return answersInterface, err
		}
		answersInterface = append(answersInterface, answers)
	}

	return answersInterface, nil
}
func insertMarked(answerIDString string, userIDString string) error {
	//QueryRow no return Rows, simple Row.

	// prevents fields with the same data from being entered (likes or favorites are not repeated)
	_, err := db.Pool.Exec(context.Background(), `
	DO
	$$
	BEGIN
	IF not EXISTS (SELECT checked
					FROM likes_answers
					WHERE users_id = `+userIDString+` AND answers_id = `+answerIDString+`) 
	THEN
	INSERT INTO likes_answers(
				users_id, answers_id)
				VALUES ( `+userIDString+`, `+answerIDString+`);
	ELSE
		RAISE NOTICE 'the field already exists';
	END IF;
	END
	$$
		`)
	if err != nil {
		return err
	}
	return nil
}
func deleteMarked(answerID int64, userID int) error {
	//QueryRow no return Rows, simple Row.
	_, err := db.Pool.Exec(context.Background(), `
	DELETE FROM likes_answers
		WHERE users_id = $1 AND  answers_id = $2
		`, userID, answerID)
	if err != nil {
		return err
	}
	return nil
}

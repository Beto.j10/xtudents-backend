package questions

import (
	"backend/api/login"
	db "backend/database"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

//Question struct query
type Question struct {
	ID              int64     `db:"id"`
	Title           string    `db:"title"`
	Content         string    `db:"content"`
	UsersID         int       `db:"users_id"`
	CategoriesID    int       `db:"categories_id"`
	QuestionDate    time.Time `db:"created_at"`
	Anonymous       bool      `db:"anonymous"`
	NickName        string    `db:"nickname"`
	CountriesID     int       `db:"countries_id"`
	Answers         int       `db:"quantity_answers"`
	Likes           int       `db:"quantity_likes"`
	LikeChecked     bool      `db:"like_checked"`
	FavoriteChecked bool      `db:"favorite_checked"`
	AnswerChecked   bool      `db:"answer_checked"`
	UserAvatar      string    `db:"user_avatar"`
	File            []byte    `db:"file"`
}

// Questions user question
func Questions(router *gin.Engine) {
	routerHome := router.Group("/question")
	// routerHome.Use(login.AuthMiddleware.MiddlewareFunc())
	{
		routerHome.GET("", func(c *gin.Context) {

			idString := c.Query("id")
			id, err := strconv.ParseInt(idString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			question, err := queryDB(id)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"question": question,
			})
		})
		routerHome.GET("/auth", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {

			userIDString := c.Query("user")
			idString := c.Query("id")
			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			id, err := strconv.ParseInt(idString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			question, err := authorizedQueryDB(id, userID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"question": question,
			})
		})

	}

}

func queryDB(id int64) (Question, error) {
	var question Question
	err := db.Pool.QueryRow(context.Background(), `
	SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id,
	questions.created_at, questions.anonymous, users.nickname, users.countries_id, users.avatar AS user_avatar,
		(SELECT COUNT(questions_id) quantity_answers
		FROM answers
		WHERE questions_id = questions.id),
		(SELECT COUNT(questions_id) quantity_likes
		FROM likes_questions
		WHERE questions_id = questions.id)
	FROM questions
		INNER JOIN users ON questions.users_id = users.id
	WHERE questions.id = $1
	`, id).Scan(&question.ID, &question.Title, &question.Content, &question.UsersID, &question.CategoriesID, &question.QuestionDate, &question.Anonymous, &question.NickName, &question.CountriesID, &question.UserAvatar, &question.Answers, &question.Likes)
	if err != nil {
		return question, err
	}

	return question, nil
}

func authorizedQueryDB(id int64, userID int) (Question, error) {

	var question Question

	err := db.Pool.QueryRow(context.Background(), `
	SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id,
	questions.created_at, questions.anonymous, users.nickname, users.countries_id, users.avatar AS user_avatar,
		(SELECT COUNT(questions_id) quantity_answers
		FROM answers
		WHERE questions_id = questions.id),
		(SELECT COUNT(questions_id) quantity_likes
		FROM likes_questions
		WHERE questions_id = questions.id),
		COALESCE((SELECT checked
		FROM likes_questions
		WHERE questions_id = questions.id AND users_id = $1
		LIMIT 1), false) AS like_checked,
		COALESCE((SELECT checked
		FROM favorites
		WHERE questions_id = questions.id AND users_id = $1
		LIMIT 1),false) AS favorite_checked,
		COALESCE((SELECT checked
		FROM answers
		WHERE questions_id = questions.id AND users_id = $1
		LIMIT 1), false) AS answer_checked
	FROM questions
		INNER JOIN users ON questions.users_id = users.id
	WHERE questions.id = $2
	`, userID, id).Scan(&question.ID, &question.Title, &question.Content, &question.UsersID, &question.CategoriesID, &question.QuestionDate, &question.Anonymous, &question.NickName, &question.CountriesID, &question.UserAvatar, &question.Answers, &question.Likes, &question.LikeChecked, &question.FavoriteChecked, &question.AnswerChecked)
	if err != nil {
		return question, err
	}

	return question, nil
}

package home

import (
	"backend/api/login"
	db "backend/database"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

//Question struct query
type Question struct {
	ID              int64     `db:"id"`
	Title           string    `db:"title"`
	Content         string    `db:"content"`
	UsersID         int       `db:"users_id"`
	CategoriesID    int       `db:"categories_id"`
	QuestionDate    time.Time `db:"created_at"`
	Anonymous       bool      `db:"anonymous"`
	NickName        string    `db:"nickname"`
	CountriesID     int       `db:"countries_id"`
	Answers         int       `db:"quantity_answers"`
	Likes           int       `db:"quantity_likes"`
	LikeChecked     bool      `db:"like_checked"`
	FavoriteChecked bool      `db:"favorite_checked"`
	AnswerChecked   bool      `db:"answer_checked"`
	UserAvatar      string    `db:"user_avatar"`
	File            []byte    `db:"file"`
}

// Home user home
func Home(router *gin.Engine) {
	routerHome := router.Group("/home")
	// routerHome.Use(login.AuthMiddleware.MiddlewareFunc())
	{
		routerHome.GET("", func(c *gin.Context) {

			category := c.Query("category")
			// categoryString := c.Query("category")
			// category, err := strconv.Atoi(categoryString)
			// if err != nil {
			// 	c.JSON(http.StatusInternalServerError, gin.H{
			// 		"message": err.Error(),
			// 	})
			// 	return
			// }
			questions, err := queryDB(category)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"questions": questions,
			})
		})
		routerHome.GET("/auth", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {

			userIDString := c.Query("user")
			category := c.Query("category")
			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			questions, err := authorizedQueryDB(category, userID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}

			c.JSON(http.StatusOK, gin.H{
				"questions": questions,
			})
		})

		// routerHome.POST("/add-marked", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {
		routerHome.POST("/add-marked", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {

			like := c.Request.FormValue("like")
			favorite := c.Request.FormValue("favorite")
			questionIDString := c.Request.FormValue("question")
			userIDString := c.Request.FormValue("user")
			fmt.Println("like: ", like, " favorite: ", favorite, " question: ", questionIDString, " user: ", userIDString)
			err := insertMarked(like, favorite, questionIDString, userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"succes": "ok"})

		})
		routerHome.POST("/delete-marked", login.AuthMiddleware.MiddlewareFunc(), func(c *gin.Context) {

			like := c.Request.FormValue("like")
			favorite := c.Request.FormValue("favorite")
			questionIDString := c.Request.FormValue("question")
			userIDString := c.Request.FormValue("user")
			fmt.Println("like: ", like, " favorite: ", favorite, " question: ", questionIDString, " user: ", userIDString)

			//convert string to int64
			questionID, err := strconv.ParseInt(questionIDString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}

			err = deleteMarked(questionID, userID, like, favorite)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"succes": "ok"})

		})

	}

	// auth := router.Group("/auth")
	// // Refresh time can be longer than token timeout
	// auth.GET("/refresh_token", login.AuthMiddleware.RefreshHandler)
	// auth.Use(login.AuthMiddleware.MiddlewareFunc())
	// {
	// 	auth.GET("/hello", helloHandler)
	// }

}

// func helloHandler(c *gin.Context) {
// 	claims := jwt.ExtractClaims(c)
// 	user, _ := c.Get(identityKey)
// 	c.JSON(200, gin.H{
// 		"userID":   claims[identityKey],
// 		"userName": user.(*User).UserName,
// 		"text":     "Hello World.",
// 	})
// }

func queryDB(category string) ([]interface{}, error) {
	var questionInterface []interface{}
	var queryFilter string
	if category == "0" {
		queryFilter = `ORDER BY created_at DESC`
	} else {
		queryFilter = "WHERE categories_id =" + category +
			`ORDER BY created_at DESC`
	}

	queryString := `
	SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id,
	questions.created_at, questions.anonymous, users.nickname, users.countries_id, users.avatar AS user_avatar, questions.file,
		(SELECT COUNT(questions_id) quantity_answers
		FROM answers
		WHERE questions_id = questions.id),
		(SELECT COUNT(questions_id) quantity_likes
		FROM likes_questions
		WHERE questions_id = questions.id)
	FROM questions
		INNER JOIN users ON questions.users_id = users.id
	` + queryFilter

	rows, err := db.Pool.Query(context.Background(), queryString)
	if err != nil {
		return questionInterface, err
	}
	defer rows.Close()

	var question Question
	for rows.Next() {
		err := rows.Scan(&question.ID, &question.Title, &question.Content, &question.UsersID, &question.CategoriesID, &question.QuestionDate, &question.Anonymous, &question.NickName, &question.CountriesID, &question.UserAvatar, &question.File, &question.Answers, &question.Likes)
		if err != nil {
			return questionInterface, err
		}
		questionInterface = append(questionInterface, question)
	}

	return questionInterface, nil
}

func authorizedQueryDB(category string, userID int) ([]interface{}, error) {
	var questionInterface []interface{}
	var queryFilter string
	if category == "0" {
		queryFilter = `ORDER BY created_at DESC`
	} else {
		queryFilter = "WHERE categories_id =" + category +
			`ORDER BY created_at DESC`
	}

	queryString := `
	SELECT questions.id, questions.title, questions.content, questions.users_id, questions.categories_id,
	questions.created_at, questions.anonymous, users.nickname, users.countries_id, users.avatar AS user_avatar, questions.file,
		(SELECT COUNT(questions_id) quantity_answers
		FROM answers
		WHERE questions_id = questions.id),
		(SELECT COUNT(questions_id) quantity_likes
		FROM likes_questions
		WHERE questions_id = questions.id),
		COALESCE((SELECT checked
		FROM likes_questions
		WHERE questions_id = questions.id AND users_id = $1
		LIMIT 1), false) AS like_checked,
		COALESCE((SELECT checked
		FROM favorites
		WHERE questions_id = questions.id AND users_id = $1
		LIMIT 1),false) AS favorite_checked,
		COALESCE((SELECT checked
		FROM answers
		WHERE questions_id = questions.id AND users_id = $1
		LIMIT 1), false) AS answer_checked
	FROM questions
		INNER JOIN users ON questions.users_id = users.id
	` + queryFilter

	rows, err := db.Pool.Query(context.Background(), queryString, userID)
	if err != nil {
		return questionInterface, err
	}
	defer rows.Close()

	var question Question
	for rows.Next() {
		err := rows.Scan(&question.ID, &question.Title, &question.Content, &question.UsersID, &question.CategoriesID, &question.QuestionDate, &question.Anonymous, &question.NickName, &question.CountriesID, &question.UserAvatar, &question.File, &question.Answers, &question.Likes, &question.LikeChecked, &question.FavoriteChecked, &question.AnswerChecked)
		if err != nil {
			return questionInterface, err
		}
		questionInterface = append(questionInterface, question)
	}

	return questionInterface, nil
}

func insertMarked(like string, favorite string, questionIDString string, userIDString string) error {
	var location string
	if like == "true" {
		location = "likes_questions"
	} else if favorite == "true" {
		location = "favorites"
	}
	//QueryRow no return Rows, simple Row.

	// prevents fields with the same data from being entered (likes or favorites are not repeated)
	_, err := db.Pool.Exec(context.Background(), `
	DO
	$$
	BEGIN
	IF not EXISTS (SELECT checked
					FROM `+location+`
					WHERE users_id = `+userIDString+` AND questions_id = `+questionIDString+`) 
	THEN
	INSERT INTO `+location+`(
				users_id, questions_id)
				VALUES ( `+userIDString+`, `+questionIDString+`);
	ELSE
		RAISE NOTICE 'the field already exists';
	END IF;
	END
	$$
		`)
	if err != nil {
		return err
	}
	return nil
}

func deleteMarked(questionID int64, userID int, like string, favorite string) error {
	var location string
	if like == "true" {
		location = "likes_questions"
	} else if favorite == "true" {
		location = "favorites"
	}
	//QueryRow no return Rows, simple Row.
	_, err := db.Pool.Exec(context.Background(), `
	DELETE FROM `+location+`
		WHERE users_id = $1 AND  questions_id = $2
		`, userID, questionID)
	if err != nil {
		return err
	}
	return nil
}

package signup

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"

	db "backend/database"
)

// Signup register user
func Signup(router *gin.Engine) {

	router.GET("/signup", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "Sign up"})
	})

	router.POST("/signup", func(c *gin.Context) {

		email := c.Request.FormValue("email")
		firstName := c.Request.FormValue("firstname")
		lastName := c.Request.FormValue("lastname")
		nickname := c.Request.FormValue("nickname")
		institution := c.Request.FormValue("institution")
		career := c.Request.FormValue("career")
		countryString := c.Request.FormValue("country")
		password := c.Request.FormValue("password")
		avatar := c.Request.FormValue("avatar")

		//convert string to int
		country, err := strconv.Atoi(countryString)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
		//encrypt password
		passwordHash, err := hashPassword(password)
		if err != nil {
			log.Println("Error generating passwordhash: ", err)
		}

		id, err := createUser(email, firstName, lastName, nickname, institution, career, country, passwordHash, avatar)
		if err != nil {
			if err.Error() == `ERROR: duplicate key value violates unique constraint "unique_email" (SQLSTATE 23505)` {
				err := errors.New("email already exists")
				c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
				return
			} else if err.Error() == `ERROR: duplicate key value violates unique constraint "unique_nickname" (SQLSTATE 23505)` {
				err := errors.New("nickname already exists")
				c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
				return

			}
			c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"id": id})

	})

}

func createUser(email string, firstName string, lastName string, nickname string, institution string, career string, country int, passwordHash string, avatar string) (int, error) {
	var id int
	//QueryRow no return Rows, simple Row.
	err := db.Pool.QueryRow(context.Background(), `
	INSERT INTO users(
		password_hash, nickname, email, institution, career, countries_id, first_name, last_name, avatar)
		VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id
		`, passwordHash, nickname, email, institution, career, country, firstName, lastName, avatar).Scan(&id)
	if err != nil {
		return 0, err
	}
	// defer rows.Close()
	// var users []interface{}
	// for rows.Next() {
	// 	err := rows.Scan(&id.id)
	// 	if err != nil {
	// 		return 0, err
	// 	}
	// 	users = append(users, id)
	// }
	return id, nil

}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

package reply

import (
	"backend/api/login"
	"context"
	"fmt"
	"net/http"
	"strconv"

	db "backend/database"

	"github.com/gin-gonic/gin"
)

//Reply user question
func Reply(router *gin.Engine) {
	routerAsk := router.Group("/reply")
	routerAsk.Use(login.AuthMiddleware.MiddlewareFunc())
	{

		routerAsk.GET("", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "Reply"})
		})

		routerAsk.POST("", func(c *gin.Context) {

			userIDString := c.Request.FormValue("user")
			questionIDString := c.Request.FormValue("question")
			answer := c.Request.FormValue("answer")

			fmt.Println("userID: ", userIDString, " questionID: ", questionIDString, " answer: ", answer)

			//convert string to int
			userID, err := strconv.Atoi(userIDString)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			//convert string to int64
			questionID, err := strconv.ParseInt(questionIDString, 10, 64)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			id, err := createAnswer(userID, questionID, answer)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
				return
			}
			c.JSON(http.StatusOK, gin.H{"id": id})

		})
	}

}

func createAnswer(userID int, questionID int64, answer string) (int64, error) {
	//QueryRow no return Rows, simple Row.

	var id int64
	err := db.Pool.QueryRow(context.Background(), `
	INSERT INTO answers(
		users_id, questions_id, answer)
		VALUES ( $1, $2, $3) RETURNING id
		`, userID, questionID, answer).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil

	// query returns the quantity of answers

	// err = db.Pool.QueryRow(context.Background(), `
	// SELECT COUNT(questions_id) AS quantity_answers
	// FROM answers
	// WHERE questions_id = $1
	// `, questionID).Scan(&quantityAnswers)
	// if err != nil {
	// 	return 0, err
	// }
	// return quantityAnswers, nil

}
